/**
 * 路线类 保存一段路线信息：路线名，起始站，终到站，距离
 */
class Path {
  constructor() {
    // 路线名
    this.lineName = ''
    
    // 起始站
    this.fromStationName = ''
    
    // 终到站
    this.toStationName = ''
    
    // 距离（站点数），默认值为很大的一个数，根据实际需要设置
    // 距离为最大值时认为不可达
    // 在实现过程中简单地以起始站到与终到站之间的站点数据为距离
    this.distance = 9999
  }
}

export default Path