/**
 * 路线类
 * 保存一条路线和路线上所有站点
 */
class Line {
  /**
   * @param {String} lineName 路线名称
   * @param {Integer} isRing  是否为环线 0=否 1=是
   */
  constructor(lineName, isRing) {
    this.name = lineName
    this.isRing = isRing
    // 路线上站名列表
    this.stationNameList = []
  }
  
  /**
   * 添加站点
   * @param {String} stationName 站名
   */
  addStationName(stationName) {
    this.stationNameList.push(stationName)
  }
}

export default Line