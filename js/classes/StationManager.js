import Station from './Station.js'

/**
 * 站点管理
 * 存放所有站点及经过该站点的路线集合
 * {
 *    XX站: {
 *      name: 'XX站',
 *      lineNameList: [xx线, yy线]
 *    },
 *    ZZ站: {...}
 * }
 * 
 */
class StationManager {
  constructor() {
    this.stationMap = new Map()
    this.stationNameSet = new Set()
  }
  
  /**
   * @param {String} stationName
   * @param {String} lineName
   */
  addLine(stationName, lineName) {
    this.stationNameSet.add(stationName)
    
    if (this.stationMap.has(stationName)) {
      this.stationMap.get(stationName).addLineName(lineName)
    } else {
      const station = new Station(stationName)
      station.addLineName(lineName)
      this.stationMap.set(stationName, station)
    }
  }
  
  /**
   * 查询经过给定起始站与终点站的路线集合
   * @param {Station} fromStation
   * @param {Station} toStation
   */
  getLineNameListWithSameFromToStation(fromStation, toStation) {
    return fromStation.lineNameList.filter(item => toStation.lineNameList.includes(item))
  }
}

export default StationManager