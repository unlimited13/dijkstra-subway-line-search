/**
 * 站点类，保存站点及经过该站点的路线
 */
class Station {
  /**
   * @param {Object} stationName 站名称
   */
  constructor(stationName) {
    this.name = stationName
    // 经过该站点的路线名
    this.lineNameList = []
  }
  
  /**
   * 添加经过该站点的路线名
   * @param {Object} lineName 路线名
   */
  addLineName(lineName) {
    this.lineNameList.push(lineName)
  }
}

export default Station