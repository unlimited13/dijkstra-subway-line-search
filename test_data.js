const data = [{
    "name": "1号线",
    "ring": 0,
    "stations": [
      "古城",
      "八角游乐园",
      "八宝山",
      "玉泉路",
      "五棵松",
      "万寿路",
      "公主坟",
      "军事博物馆",
      "木樨地南",
      "礼士路",
      "复兴门",
      "西单",
      "天安门西",
      "天安门东",
      "王府井",
      "东单",
      "建国门",
      "永安里",
      "国贸",
      "大望路",
      "四惠",
      "四惠东",
      "高碑店",
      "传媒大学",
      "双桥",
      "管庄",
      "八里桥",
      "通州北苑",
      "果园",
      "九棵树",
      "梨园",
      "临河里",
      "土桥",
      "花庄",
      "环球度假区"
    ]
  },
  {
    "name": "2号线",
    "ring": 1,
    "stations": [
      "西直门",
      "积水潭",
      "鼓楼大街",
      "安定门",
      "雍和宫",
      "东直门",
      "东四十条",
      "朝阳门",
      "建国门",
      "北京站",
      "崇文门",
      "前门",
      "和平门",
      "宣武门",
      "长椿街",
      "复兴门",
      "阜成门",
      "车公庄"
    ]
  },
  {
    "name": "4号线",
    "ring": 0,
    "stations": [
      "安河桥北",
      "北宫门",
      "西苑",
      "圆明园",
      "北京大学东门",
      "中关村",
      "海淀黄庄",
      "人民大学",
      "魏公村",
      "国家图书馆",
      "动物园",
      "西直门",
      "新街口",
      "平安里",
      "西四",
      "灵境胡同",
      "西单",
      "宣武门",
      "菜市口",
      "陶然亭",
      "北京南站",
      "马家堡",
      "角门西",
      "公益西桥",
      "新宫",
      "西红门",
      "高米店北",
      "高米店南",
      "枣园",
      "清源路",
      "黄村西大街",
      "黄村火车站",
      "义和庄",
      "生物医药基地",
      "天宫院"
    ]
  },
  {
    "name": "5号线",
    "ring": 0,
    "stations": [
      "天通苑北",
      "天通苑",
      "天通苑南",
      "立水桥",
      "立水桥南",
      "北苑路北",
      "大屯路东",
      "惠新西街北口",
      "惠新西街南口",
      "和平西桥",
      "和平里北街",
      "雍和宫",
      "北新桥",
      "张自忠路",
      "东四",
      "灯市口",
      "东单",
      "崇文门",
      "磁器口",
      "天坛东门",
      "蒲黄榆",
      "刘家窑",
      "宋家庄"
    ]
  },
  {
    "name": "6号线",
    "ring": 0,
    "stations": [
      "金安桥",
      "苹果园",
      "杨庄西",
      "黄村",
      "廖公庄",
      "田村",
      "海淀五路居",
      "慈寿寺",
      "花园桥",
      "白石桥南",
      "车公庄西",
      "车公庄",
      "平安里",
      "北海北",
      "南锣鼓巷",
      "东四",
      "朝阳门",
      "东大桥",
      "呼家楼",
      "金台路",
      "十里堡",
      "青年路",
      "褡裢坡",
      "黄渠",
      "常营",
      "草房",
      "物资学院路",
      "通州北关",
      "北运河西",
      "郝家府",
      "东夏园",
      "潞城"
    ]
  },
  {
    "name": "7号线",
    "ring": 0,
    "stations": [
      "北京西站",
      "湾子",
      "达官营",
      "广安门内",
      "菜市口",
      "虎坊桥",
      "珠市口",
      "桥湾",
      "磁器口",
      "广渠门内",
      "广渠门外",
      "九龙山",
      "大郊亭",
      "百子湾",
      "化工",
      "南楼梓庄",
      "欢乐谷景区",
      "双合",
      "焦化厂",
      "黄厂",
      "郎辛庄",
      "黑庄户",
      "万盛西",
      "万盛东",
      "群芳",
      "高楼金",
      "花庄",
      "环球度假区"
    ]
  },
  {
    "name": "8号线",
    "ring": 0,
    "stations": [
      "朱辛庄",
      "育知路",
      "平西府",
      "回龙观东大街",
      "霍营",
      "育新",
      "西小口",
      "永泰庄",
      "林萃桥",
      "森林公园南门",
      "奥林匹克公园",
      "奥体中心",
      "北土城",
      "安华桥",
      "安德里北街",
      "鼓楼大街",
      "什刹海",
      "南锣鼓巷",
      "中国美术馆",
      "金鱼胡同",
      "王府井",
      "前门",
      "珠市口",
      "天桥",
      "永定门外",
      "木樨园",
      "海户屯",
      "大红门南",
      "和义",
      "东高地",
      "火箭万源",
      "五福堂",
      "德茂",
      "瀛海"
    ]
  },
  {
    "name": "9号线",
    "ring": 0,
    "stations": [
      "国家图书馆",
      "白石桥南",
      "白堆子",
      "军事博物馆",
      "北京西站",
      "六里桥东",
      "六里桥",
      "七里庄",
      "丰台东大街",
      "丰台南路",
      "科怡路",
      "丰台科技园",
      "郭公庄"
    ]
  },
  {
    "name": "10号线",
    "ring": 1,
    "stations": [
      "丰台站",
      "泥洼",
      "西局",
      "六里桥",
      "莲花桥",
      "公主坟",
      "西钓鱼台",
      "慈寿寺",
      "车道沟",
      "长春桥",
      "火器营",
      "巴沟",
      "苏州街",
      "海淀黄庄",
      "知春里",
      "知春路",
      "西土城",
      "牡丹园",
      "健德门",
      "北土城",
      "安贞门",
      "惠新西街南口",
      "芍药居",
      "太阳宫",
      "三元桥",
      "亮马桥",
      "农业展览馆",
      "团结湖",
      "呼家楼",
      "金台夕照",
      "国贸",
      "双井",
      "劲松",
      "潘家园",
      "十里河",
      "分钟寺",
      "成寿寺",
      "宋家庄",
      "石榴庄",
      "大红门",
      "角门东",
      "角门西",
      "草桥",
      "纪家庙",
      "首经贸"
    ]
  },
  {
    "name": "11号线",
    "ring": 0,
    "stations": [
      "金安桥",
      "北辛安",
      "新首钢"
    ]
  },
  {
    "name": "13号线",
    "ring": 0,
    "stations": [
      "西直门",
      "大钟寺",
      "知春路",
      "五道口",
      "上地",
      "西二旗",
      "龙泽",
      "回龙观",
      "霍营",
      "立水桥",
      "北苑",
      "望京西",
      "芍药居",
      "光熙门",
      "柳芳",
      "东直门"
    ]
  },
  {
    "name": "14号线",
    "ring": 0,
    "stations": [
      "张郭庄",
      "园博园",
      "大瓦窑",
      "郭庄子",
      "大井",
      "七里庄",
      "西局",
      "东管头",
      "丽泽商务区",
      "菜户营",
      "西铁营",
      "景风门",
      "北京南站",
      "永定门外",
      "景泰",
      "蒲黄榆",
      "方庄",
      "十里河",
      "北工大西门",
      "平乐园",
      "九龙山",
      "大望路",
      "金台路",
      "朝阳公园",
      "枣营",
      "东风北桥",
      "将台",
      "望京南",
      "阜通",
      "望京",
      "东湖渠",
      "来广营",
      "善各庄"
    ]
  },
  {
    "name": "15号线",
    "ring": 0,
    "stations": [
      "清华东路西口",
      "六道口",
      "北沙滩",
      "奥林匹克公园",
      "安立路",
      "大屯路东",
      "关庄",
      "望京西",
      "望京",
      "望京东",
      "崔各庄",
      "马泉营",
      "孙河",
      "国展",
      "花梨坎",
      "后沙峪",
      "南法信",
      "石门",
      "顺义",
      "俸伯"
    ]
  },
  {
    "name": "16号线",
    "ring": 0,
    "stations": [
      "北安河",
      "温阳路",
      "稻香湖路",
      "屯佃",
      "永丰",
      "永丰南",
      "西北旺",
      "马连洼",
      "农大南路",
      "西苑",
      "万泉河桥",
      "苏州桥",
      "万寿寺",
      "国家图书馆",
      "甘家口",
      "玉渊潭东门"
    ]
  },
  {
    "name": "17号线",
    "ring": 0,
    "stations": [
      "十里河",
      "周家庄",
      "十八里店",
      "北神树",
      "次渠",
      "北次渠",
      "嘉会湖"
    ]
  },
  {
    "name": "19号线",
    "ring": 0,
    "stations": [
      "牡丹园",
      "积水潭",
      "牛街",
      "草桥",
      "新发地",
      "新宫"
    ]
  },
  {
    "name": "八通线",
    "ring": 0,
    "stations": [
      "四惠",
      "四惠东",
      "高碑店",
      "传媒大学",
      "双桥",
      "管庄",
      "八里桥",
      "通州北苑",
      "果园",
      "九棵树",
      "梨园",
      "临河里",
      "土桥"
    ]
  },
  {
    "name": "昌平线",
    "ring": 0,
    "stations": [
      "昌平西山口",
      "十三陵景区",
      "昌平",
      "昌平东关",
      "北邵洼",
      "南邵",
      "沙河高教园",
      "沙河",
      "巩华城",
      "朱辛庄",
      "生命科学园",
      "西二旗",
      "清河站"
    ]
  },
  {
    "name": "大兴机场线",
    "ring": 0,
    "stations": [
      "草桥",
      "大兴新城",
      "大兴机场"
    ]
  },
  {
    "name": "亦庄线",
    "ring": 0,
    "stations": [
      "宋家庄",
      "肖村",
      "小红门",
      "旧宫",
      "亦庄桥",
      "亦庄文化园",
      "万源街",
      "荣京东街",
      "荣昌东街",
      "同济南路",
      "经海路",
      "次渠南",
      "次渠",
      "亦庄火车站"
    ]
  },
  {
    "name": "亦庄线T1线",
    "ring": 0,
    "stations": [
      "定海园",
      "定海园西",
      "经海一路（京东方）",
      "亦创会展中心",
      "荣昌东街（T1）",
      "亦庄同仁",
      "鹿圈东",
      "泰河路",
      "九号村（奔驰北）",
      "四海庄",
      "太和桥北",
      "瑞合庄",
      "融兴街",
      "屈庄（奔驰南）"
    ]
  },
  {
    "name": "房山线",
    "ring": 0,
    "stations": [
      "阎村东",
      "苏庄",
      "良乡南关",
      "良乡大学城西",
      "良乡大学城",
      "良乡大学城北",
      "广阳城",
      "篱笆房",
      "长阳",
      "稻田",
      "大葆台",
      "郭公庄",
      "白盆窑",
      "花乡东桥",
      "首经贸",
      "东管头南"
    ]
  },
  {
    "name": "s1线",
    "ring": 0,
    "stations": [
      "石厂",
      "小园",
      "栗园庄",
      "上岸",
      "桥户营",
      "四道桥",
      "金安桥",
      "苹果园"
    ]
  },
  {
    "name": "s2线",
    "ring": 0,
    "stations": [
      "延庆",
      "八达岭",
      "南口",
      "黄土店"
    ]
  },
  {
    "name": "s2线主线",
    "ring": 0,
    "stations": [
      "沙城",
      "康庄",
      "八达岭",
      "南口",
      "黄土店"
    ]
  },
  {
    "name": "燕房线",
    "ring": 0,
    "stations": [
      "燕山",
      "房山城关",
      "饶乐府",
      "马各庄",
      "大石河东",
      "星城",
      "阎村",
      "紫草坞",
      "阎村东"
    ]
  },
  {
    "name": "西郊线",
    "ring": 0,
    "stations": [
      "香山",
      "植物园",
      "万安",
      "茶棚",
      "颐和园西门",
      "巴沟"
    ]
  },
  {
    "name": "机场线",
    "ring": 0,
    "stations": [
      "北新桥",
      "东直门",
      "三元桥",
      "T3航站楼",
      "T2航站楼"
    ]
  }
]

export default data
