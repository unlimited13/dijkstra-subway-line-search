## 基于Dijkstra算法，JavaScript实现地铁换乘路线查询

### 使用方式(参考index.html中使用方式, 数据格式参考test_data.js)

#### 1.地铁线路数据格式
```javascript
[
  {
    name: '线路名称',
    ring: '是否环线 0=否 1=是',
    stations: ['站1', '站2', ... ,...]
  },
  {...},
  ...
]
示例：
[{
    "name": "1号线",
    "ring": 0,
    "stations": [
      "古城",
      "八角游乐园",
      "八宝山",
      "玉泉路",
      "五棵松",
      "万寿路",
      "公主坟",
      "军事博物馆",
      "木樨地南",
      "礼士路",
      "复兴门",
      "西单",
      "天安门西",
      "天安门东",
      "王府井",
      "东单",
      "建国门",
      "永安里",
      "国贸",
      "大望路",
      "四惠",
      "四惠东",
      "高碑店",
      "传媒大学",
      "双桥",
      "管庄",
      "八里桥",
      "通州北苑",
      "果园",
      "九棵树",
      "梨园",
      "临河里",
      "土桥",
      "花庄",
      "环球度假区"
    ]
  },
  {
    "name": "2号线",
    "ring": 1,
    "stations": [
      "西直门",
      "积水潭",
      "鼓楼大街",
      "安定门",
      "雍和宫",
      "东直门",
      "东四十条",
      "朝阳门",
      "建国门",
      "北京站",
      "崇文门",
      "前门",
      "和平门",
      "宣武门",
      "长椿街",
      "复兴门",
      "阜成门",
      "车公庄"
    ]
  }
]
```

#### 2.引入Dijstra
```javascript
import Dijkstra from './js/Dijkstra.js'
```


#### 3.初始化数据
```javascript
Dijkstra.initData(data) // data为符合1中格式的地铁线路数据
```

#### 4.查询线路
```javascript
Dijkstra.searchPath(startStation, endStation) // startStation：起始站名 endStation：终点站名
```

返回数据格式
```javascript
// 换乘路线与车站集合
[
    {
        "distance": 16,                 // 在本条线路上经过的站点数
        "fromStationName": "古城",      // 本条线路上起点站
        "lineName": "1号线",            // 线路名称
        "toStationName": "建国门",      // 本条线路上换乘站
        "stationList": [                // 本条线路上经过的车站集合
            "古城",
            "八角游乐园",
            "八宝山",
            "玉泉路",
            "五棵松",
            "万寿路",
            "公主坟",
            "军事博物馆",
            "木樨地南",
            "礼士路",
            "复兴门",
            "西单",
            "天安门西",
            "天安门东",
            "王府井",
            "东单",
            "建国门"
        ]
    },
    {
        "distance": 2,
        "fromStationName": "建国门",
        "lineName": "2号线",
        "toStationName": "东四十条",
        "stationList": [
            "建国门",
            "朝阳门",
            "东四十条"
        ]
    }
]
```



> 本程序为练习算法所写，只简单以站点数作为乘坐距离。作者水平有限，可能存在一些未知Bug，如需使用请谨慎处理